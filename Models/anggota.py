from odoo import api, fields, models, _

class Partner(models.Model) :
    _inherit = 'res.partner'

    penulis = fields.Boolean(string="Author ?")
    anggota = fields.Boolean(string="Member ?")
    penerbit = fields.Boolean(string="Publisher ?")
    born_date = fields.Date(string="Date Of Birth")
    death_date = fields.Date(string="Death Of Date")
    biography = fields.Text(string="Biography")
    bhs = fields.Selection(selection='_get_bhs', string="Language")

    _sql_constraints = [('name_uniq', 'UNIQUE (name)', 'Name must unique !')]
 
    @api.model
    def _get_bhs(self):
        return self.env['res.lang'].get_installed()

class StockLocation(models.Model) :
    _inherit = 'stock.location'

    book_location = fields.Boolean(string="Location Book ?")