from odoo import api, fields, models, _
from odoo.exceptions import UserError,ValidationError

class Product(models.Model) :
    _inherit = 'product.product'

    isbn = fields.Char(string="ISBN Code", unique=True)
    catalog_num = fields.Char(string="Catalog Number")
    bhs = fields.Char(string="Language", selection='_get_bhs')
    author_id = fields.Many2one('res.partner', string="Author", domain=[('penulis', '=', True)])
    publisher_id = fields.Many2one('res.partner', string="Publisher", domain=[('penerbit', '=', True)])
    no_page = fields.Integer(string="Number Of Page")
    location_id = fields.Many2one('stock.location', string="Location", domain=[('book_location', '=', True)])
    num_edition = fields.Integer(string="No.Edition")
    resensi = fields.Text(string="Resensi")
    state = fields.Selection([('available', 'Available'), ('rent', 'Rented')], string="State", readonly=True, default='available')

    _sql_constraints = [
        ('unique_barcode', 'unique(barcode)', 'barcode field must be unique across all the products'),
        ('code_uniq', 'unique (default_code)', 'Code of the product must be unique !')
    ]

    def unlink(self) :
        if self.state != 'available' :
            raise UserError(("Buku tidak bisa dihapus pada state %s !") % (self.state))

    @api.model
    def _get_bhs(self) :
        return self.env['res.leng'].get_installed()
    
    @api.model
    def create(self, vals) :
        vals['default_code'] = self.env['ir.sequence'].next_by_code('product.product')
        return super(Product, self).create(vals)