from odoo import api, fields, models, _
from odoo.exceptions import UserError,ValidationError
from datetime import timedelta, datetime, date

class KartuPeserta(models.Model) :
    _name = 'kartu.peserta'

    name = fields.Char(string="Reference", readonly=True, default='/')
    partner_id = fields.Many2one('res.partner', string="Liblary Card", readonly=True, domain=[('anggota', '=', True)], required=True, 
                  states={'draft' : [('readonly', False)]})
    kartu_line = fields.One2many('kartu.peserta.line', 'kartu_id', string="Member Card", readonly=True, states={'confrim' : [('readonly', False)]})
    state = fields.Selection([('draft', 'Draft'), ('confrim', 'Confrim')], string="State", default='draft', readonly=True)
    note = fields.Text(string="Notes")

    def action_confrim(self) :
        return self.write({'state' : 'confrim'})
    
    def nyoba(self) :
        raise ValidationError("Di Klik")

    @api.model
    def create(self, vals) :
        vals['name'] = self.env['ir.sequence'].next_by_code('kartu.peserta')
        return super(KartuPeserta, self).create(vals)
  
    def unlink(self) :
        if self.state != 'draft' :
            raise UserError(("Kartu pinjaman tidak bisa dihapus pada state %s !") % (self.state))
    
    def action_print_kartu(self) :
        return self.env.ref('Perpustakaan.report_kartu_action').report_action(self)

class KartuPesertaLine(models.Model) :
    _name = 'kartu.peserta.line'

    kartu_id = fields.Many2one('kartu.peserta', string="ID Liblary Card", required=True, ondelete="cascade")
    product_id = fields.Many2one('product.product', string="Book", required=True)
    state = fields.Selection([('draft', 'Draft'), ('rent', 'Rented'), ('return', 'Done')], string="State", readonly=True, default='draft')
    start_date = fields.Date(string="Start Date", default=fields.Date.context_today)
    end_date = fields.Date(string="End Date", compute='get_end_date', inverse='set_end_date', readonly=True, store=True)
    duration = fields.Integer(string="Duration (Days)",  default=7)
    partner_id = fields.Many2one('res.partner', string="Member", related="kartu_id.partner_id", store=True, readonly=True)
    name_card = fields.Char(string="Card", related="kartu_id.name",readonly=True, store=True)
    
    def unlink(self):
        if self.state != 'draft':
            raise exceptions.UserError(("Data buku pinjaman tidak bisa dihapus pada state %s !") % (self.state))
        return super(KartuPesertaLine, self).unlink()
    
    def kembali(self) :
        self.product_id.state = 'available'
        return self.write({'state' : 'return'})
    
    def pinjam(self) :
        self.product_id.state = 'rent'
        return self.write({'state' : 'rent'})
    

    def get_duration(self) :
        for sesi in self :
            if not (sesi.start_date and sesi.end_date) :
                continue
            start_date = fields.Datetime.from_string(sesi.start_date)
            end_date = fields.Datetime.from_string(sesi.end_date)
            sesi.duration = (end_date - start_date).days + 1
    
    @api.depends('duration', 'start_date')
    def get_end_date(self) :
        for sesi in self :
            if not sesi.start_date :
                sesi.end_date = sesi.start_date
                continue
            start = fields.Date.from_string(sesi.start_date)
            sesi.end_date = start + timedelta(days=sesi.duration)