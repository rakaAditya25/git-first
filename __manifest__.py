{
    'name' : "Perpustakaan",
    'version' : "1.0",
    'depends' : ['base', 'mail', 'report_xlsx', 'purchase', 'purchase_requisition', 
    
    'stock', 'product', 'contacts'
    
    ], #raka nambahin partner di depends
    'author' : "raka@gmail.com",
    'category' : "Education",
    'description' : """Deskripsi""",

    'data' : [
        'Security/security.xml',
        'Security/ir.model.access.csv',
        'report/report_action.xml',
        'report/format_report_kartu.xml',
        'Views/buku.xml',
        'Views/anggota.xml',
        'Views/loan.xml',
        'Views/list_loan.xml',
        'Views/available.xml',
        'Views/card.xml',
        'Views/rak.xml',
        'Views/author.xml',
        'Views/publisher.xml',
        'Views/kategori.xml',
        'Views/sequence_data.xml',
        'Views/menu.xml',
    ],

    'demo' : [
        #'demo/demo.xml'
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}